<?php
interface SecretKeyStorage
{
    public function getSecretKey(): string;
}

/**
 * Class FileStorage
 *
 * Implements the SecretKeyStorage interface to provide methods 
 * for storing and retrieving secret keys.
 */
class FileStorage implements SecretKeyStorage
{
    private $filePath;

    public function __construct(string $filePath)
    {
        $this->filePath = $filePath;
    }

    public function getSecretKey(): string
    {
        // Read the secret key from the file
        $fileContents = file_get_contents($this->filePath);
        return $fileContents;
    }
}

class DatabaseStorage implements SecretKeyStorage
{
    private $connection;

    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    public function getSecretKey(): string
    {
        // Retrieve the secret key from the database
        $query = "SELECT secret_key FROM secret_keys";
        $result = $this->connection->query($query);
        $row = $result->fetch(PDO::FETCH_ASSOC);
        return $row['secret_key'];
    }
}

class RedisStorage implements SecretKeyStorage
{
    private $redisClient;

    public function __construct(Redis $redisClient)
    {
        $this->redisClient = $redisClient;
    }

    public function getSecretKey(): string
    {
        // Retrieve the secret key from Redis
        return $this->redisClient->get('secret_key');
    }
}

class CloudStorage implements SecretKeyStorage
{
    private $cloudClient;

    public function __construct(CloudClient $cloudClient)
    {
        $this->cloudClient = $cloudClient;
    }

    public function getSecretKey(): string
    {
        // Retrieve the secret key from the cloud storage
        return $this->cloudClient->getSecretKey();
    }
}

class UserData
{
    private $storage;

    public function __construct(SecretKeyStorage $storage)
    {
        $this->storage = $storage;
    }

    public function getUserData(string $userId): array
    {
        // Retrieve the secret key from the storage
        $secretKey = $this->storage->getSecretKey();

        // Make the API request using the secret key
        $response = $this->makeApiRequest($userId, $secretKey);

        // Process the response and return the user data
        return $response['data'];
    }

    private function makeApiRequest(string $userId, string $secretKey): array
    {
        // Make the API request using the secret key
        // ...

        // Return the API response
        return [
            'data' => [
                // User data
            ]
        ];
    }
}

// Usage example
$fileStorage = new FileStorage('/path/to/secret_key.txt');
$databaseStorage = new DatabaseStorage($pdoConnection);
$redisStorage = new RedisStorage($redisClient);
$cloudStorage = new CloudStorage($cloudClient);

$userDataWithFileStorage = new UserData($fileStorage);
$userDataWithDatabaseStorage = new UserData($databaseStorage);
$userDataWithRedisStorage = new UserData($redisStorage);
$userDataWithCloudStorage = new UserData($cloudStorage);

$userDataWithFileStorage->getUserData('123');
$userDataWithDatabaseStorage->getUserData('456');
$userDataWithRedisStorage->getUserData('789');
$userDataWithCloudStorage->getUserData('abc');