<?php
interface HttpService
{
    public function request(string $url, string $method, array $options = []);
}

class XMLHttpService implements HttpService
{
    public function request(string $url, string $method, array $options = [])
    {
    }
}

class Http
{
    private $service;

    public function __construct(HttpService $httpService)
    {
        $this->service = $httpService;
    }

    public function get(string $url, array $options)
    {
        $this->service->request($url, 'GET', $options);
    }

    public function post(string $url, array $options)
    {
        $this->service->request($url, 'POST', $options);
    }
}

// Использование
$xmlHttpService = new XMLHttpService();
$http = new Http($xmlHttpService);
$http->get('http://example.com/api', ['param1' => 'value1']);
$http->post('http://example.com/api', ['param2' => 'value2']);
