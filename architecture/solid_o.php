<?php

class SomeObject
{
    protected $name;

    public function __construct(string $name)
    { 
    }

    public function getObjectName()
    { 
    }
}

interface ObjectHandler
{
    public function handle($object): bool;
}

class Object1Handler implements ObjectHandler
{
    public function handle($object): bool
    {
        return $object->getObjectName() == 'object_1';
    }
}

class Object2Handler implements ObjectHandler
{
    public function handle($object): bool
    {
        return $object->getObjectName() == 'object_2';
    }
}

class SomeObjectsHandler
{
    private $handlers = [];

    public function __construct()
    {
        $this->handlers = [
            new Object1Handler(),
            new Object2Handler()
        ];
    }

    public function handleObjects(array $objects): array
    {
        $handledObjects = [];
        foreach ($objects as $object) {
            foreach ($this->handlers as $handler) {
                if ($handler->handle($object)) {
                    $handledObjects[] = get_class($handler);
                    break;
                }
            }
        }

        return $handledObjects;
    }
}

$objects = [
    new SomeObject('object_1'),
    new SomeObject('object_2')
];

$soh = new SomeObjectsHandler();
$soh->handleObjects($objects);