<?php

$array = [
    ["id" => 1, "date" => "12.01.2020", "name" => "test1"],
    ["id" => 2, "date" => "02.05.2020", "name" => "test2"],
    ["id" => 4, "date" => "08.03.2020", "name" => "test4"],
    ["id" => 1, "date" => "22.01.2020", "name" => "test1"],
    ["id" => 2, "date" => "11.11.2020", "name" => "test4"],
    ["id" => 3, "date" => "06.06.2020", "name" => "test3"],
];


// №1
// Т.к. желательно не использовать foreach, используем функцию, которая
// имитирует его поведение, я взял array_reduce, т.к. она учитывает значение
// предыдущей итерации. В конечный массив попадут только уникальные значения
$uniqueArray = array_reduce(
    $array, function ($carry, $item) {
        if (!isset($carry[$item['id']])) {
            $carry[$item['id']] = $item;
        }
        return $carry;
    }, []
);

// Посмотреть результат
// foreach ($uniqueArray as $record) {
//     echo "ID: " . $record['id'] . ", Name: " . $record['name'] . ", Date: " . $record['date'] . PHP_EOL;
// }

// -------------------------------------------------------------------------- //

// №2
// Изменяя целевую колонку, мы можем изменить поле сортировки
$sortValues = array_column($array, 'id');

// Сортируем массив, опираясь на значения выборки
array_multisort($sortValues, SORT_ASC, $array);

// Посмотреть результат
// foreach ($array as $record) {
//     echo "ID: " . $record['id'] . ", Name: " . $record['name'] . ", Date: " . $record['date'] . PHP_EOL;
// }


// -------------------------------------------------------------------------- //
// №3 вернуть из массива только элементы, удовлетворяющие внешним условиям (например элементы с определенным id)
// Фильтруем массив для поиска всех совпадений
$searchId = 2;

$filteredArray = array_filter(
    $array, function ($record) use ($searchId) {
        return $record['id'] === $searchId;
    }
);

// Output the filtered records
// foreach ($filteredArray as $record) {
//     echo "ID: " . $record['id'] . ", Name: " . $record['name'] . ", Date: " . $record['date'] . PHP_EOL;
// }

// -------------------------------------------------------------------------- //
// №4 изменить в массиве значения и ключи (использовать name => id в качестве пары ключ => значение)
$newArray = array_reduce(
    $array, function ($carry, $item) {
        $carry[$item['name']] = $item['id'];
        return $carry;
    }, []
);

// Результат
// print_r($newArray);

// -------------------------------------------------------------------------- //

// №5

/* Этот запрос получит id и name всех продуктов, имеющих все возможные теги в базе данных. Он объединяет таблицы goods, goods_tags и tags, группирует результаты по id и name продукта,
а затем фильтрует результаты, чтобы оставить только те продукты, которые имеют такое же количество различных тегов, что и общее количество тегов в таблице тегов
*/

/* SELECT g.id, g.name
FROM goods g
JOIN goods_tags gt ON g.id = gt.goods_id
JOIN tags t ON gt.tag_id = t.id
GROUP BY g.id, g.name
HAVING COUNT(DISTINCT t.id) = (SELECT COUNT(*) FROM tags); */

// №6

/*
Без подзапроса не получается

SELECT DISTINCT department_id
FROM evaluations
WHERE gender = true
AND value > 5
GROUP BY department_id
HAVING COUNT(*) = (SELECT COUNT(*) FROM evaluations e2 WHERE e2.department_id = evaluations.department_id AND e2.gender = true);
*/

